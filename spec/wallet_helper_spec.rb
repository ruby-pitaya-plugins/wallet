require 'spec_helper'

RSpec.describe 'WalletHelper', type: :request do
  context 'create_player_wallet' do
    it 'success' do
      config = {
        'plugin/wallet/initial_player_wallet' => {
          gold: 100,
          gems: 15,
        }
      }

      user = User.create
      wallet_helper = Wallet::WalletHelper.new(config)
      player_wallet = wallet_helper.get_or_create_player_wallet(user.id)

      expect(player_wallet.currency_map['gold']).to eq(100)
      expect(player_wallet.currency_map['gems']).to eq(15)
    end
  end

  context 'get_player_wallet' do
    it 'success' do
      config = {}

      user = User.create
      Wallet::PlayerWallet.create(
        user: user,
        currency_map: {
          gold: 11,
          gems: 1,
        }
      )

      wallet_helper = Wallet::WalletHelper.new(config)
      player_wallet = wallet_helper.get_or_create_player_wallet(user.id)

      expect(player_wallet.currency_map['gold']).to eq(11)
      expect(player_wallet.currency_map['gems']).to eq(1)
    end
  end

  context 'add_currency' do
    it 'success' do
      config = {
        'plugin/wallet/initial_player_wallet' => {
          gold: 100,
          gems: 15,
        },
        'plugin/wallet/currency_names' => [
          'gold',
          'gems',
        ]
      }

      user = User.create
      wallet_helper = Wallet::WalletHelper.new(config)
      player_wallet = wallet_helper.get_or_create_player_wallet(user.id)

      wallet_helper.add_currency(player_wallet, 'gold', 25)
      wallet_helper.add_currency(player_wallet, 'gems', 10)

      expect(player_wallet.currency_map['gold']).to eq(125)
      expect(player_wallet.currency_map['gems']).to eq(25)
    end

    it 'error_invalid_currency_code' do
      config = {
        'plugin/wallet/initial_player_wallet' => {
          gold: 100,
          gems: 15,
        },
        'plugin/wallet/currency_names' => [
          'gold',
          'gems',
        ]
      }

      user = User.create
      wallet_helper = Wallet::WalletHelper.new(config)
      player_wallet = wallet_helper.get_or_create_player_wallet(user.id)

      expect {

        wallet_helper.add_currency(player_wallet, 'another_currency', 1)

      }.to raise_error(Wallet::InvalidCurrencyCodeError)
    end
  end

  context 'remove_currency' do
    it 'success' do
      config = {
        'plugin/wallet/initial_player_wallet' => {
          gold: 100,
          gems: 15,
        },
        'plugin/wallet/currency_names' => [
          'gold',
          'gems',
        ]
      }

      user = User.create
      wallet_helper = Wallet::WalletHelper.new(config)
      player_wallet = wallet_helper.get_or_create_player_wallet(user.id)

      wallet_helper.remove_currency(player_wallet, 'gold', 25)
      wallet_helper.remove_currency(player_wallet, 'gems', 10)

      expect(player_wallet.currency_map['gold']).to eq(75)
      expect(player_wallet.currency_map['gems']).to eq(5)
    end

    it 'error_invalid_currency_code' do
      config = {
        'plugin/wallet/initial_player_wallet' => {
          gold: 100,
          gems: 15,
        },
        'plugin/wallet/currency_names' => [
          'gold',
          'gems',
        ]
      }

      user = User.create
      wallet_helper = Wallet::WalletHelper.new(config)
      player_wallet = wallet_helper.get_or_create_player_wallet(user.id)

      expect {

        wallet_helper.remove_currency(player_wallet, 'another_currency', 1)

      }.to raise_error(Wallet::InvalidCurrencyCodeError)
    end
  end
end