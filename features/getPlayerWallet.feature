Feature: Get player wallet

  As a player I want to get my wallet

  Background:
    Given the following user:
      | user_id                              |
      | 00000000-0000-0000-0000-000000000001 |
    And the user '00000000-0000-0000-0000-000000000001' is authenticated
    And config is the following json:
      """
      {
        "plugin/wallet/initial_player_wallet": {
          "gold": 100,
          "gems": 10
        }
      }
      """

  Scenario: Get initial player wallet
    Given client call route 'rubypitaya.walletHandler.getPlayerWallet'
    Then server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "currencyMap": {
            "gold": 100,
            "gems": 10
          }
        }
      }
      """

  Scenario: Get player wallet
    Given the following wallet:
      | gold | gems | user_id                              |
      | 123  | 12   | 00000000-0000-0000-0000-000000000001 |
    Given client call route 'rubypitaya.walletHandler.getPlayerWallet'
    Then server should response the following json:
      """
      {
        "code": "RP-200",
        "data": {
          "currencyMap": {
            "gems": 12,
            "gold": 123
          }
        }
      }
      """

