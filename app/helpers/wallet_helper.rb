module Wallet

  class WalletHelper

    def initialize(config)
      @config = config
    end

    def get_or_create_player_wallet(user_id)
      player_wallet = PlayerWallet.find_by_user_id(user_id)
      player_wallet ||= PlayerWallet.new(
        user_id: user_id,
        currency_map: @config['plugin/wallet/initial_player_wallet'],
      )

      return player_wallet
    end

    def add_currency(player_wallet, currency_code, amount)
      check_error_invalid_currency_code(currency_code)

      player_wallet.increment_currency(currency_code, amount)
    end

    def remove_currency(player_wallet, currency_code, amount)
      check_error_invalid_currency_code(currency_code)

      player_wallet.decrement_currency(currency_code, amount)
    end
    
    private

    def is_valid_currency_code?(currency_code)
      @config['plugin/wallet/currency_names'].include?(currency_code)
    end

    def check_error_invalid_currency_code(currency_code)
      return if is_valid_currency_code?(currency_code)

      message = StatusCodes.message_error_invalid_currency_code(currency_code)
      raise InvalidCurrencyCodeError.new(message)
    end
  end
end