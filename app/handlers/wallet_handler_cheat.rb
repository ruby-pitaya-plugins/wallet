module Wallet

  class WalletHandlerCheat < RubyPitaya::HandlerBase

    non_authenticated_actions :authenticate

    def authenticate
      user_id = @params[:userId]

      user = User.find_by_id(user_id)
      user ||= User.create!

      @session.uid = user.id
  
      bind_session_response = @postman.bind_session(@session)
  
      unless bind_session_response.dig(:error, :code).nil?
        return response = {
          code: RubyPitaya::StatusCodes::CODE_AUTHENTICATION_ERROR,
          msg: 'Error to authenticate',
        }
      end
  
      response = {
        code: StatusCodes::CODE_OK,
      }
    end

    def addCurrency
      user_id = @params[:userId]
      amount = @params[:amount]
      currency_code = @params[:currencyCode]

      user_id ||= @session.uid

      player_wallet = helper.get_or_create_player_wallet(user_id)
      helper.add_currency(player_wallet, currency_code, amount)

      player_wallet.save!

      response = {
        code: StatusCodes::CODE_OK,
        data: player_wallet.to_hash,
      }
    end

    private

    def helper
      @objects[:helper]
    end
  end
end