module Wallet

  class WalletHandler < RubyPitaya::HandlerBase

    def getPlayerWallet
      user_id = @session.uid

      player_wallet = helper.get_or_create_player_wallet(user_id)

      response = {
        code: StatusCodes::CODE_OK,
        data: player_wallet.to_hash,
      }
    end

    private

    def helper
      @objects[:helper]
    end
  end
end