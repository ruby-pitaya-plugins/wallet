require 'active_record'

module Wallet
  class PlayerWallet < ActiveRecord::Base

    belongs_to :user

    validates_presence_of :user

    def to_hash
      {
        currencyMap: currency_map,
      }
    end

    def get_currency(currency_code)
      return 0 unless currency_map.key?(currency_code)
      currency_map[currency_code]
    end

    def has_currency?(currency_code, value)
      get_currency(currency_code) >= value
    end

    def increment_currency(currency_code, value)
      self.currency_map[currency_code] ||= 0
      self.currency_map[currency_code] += value
    end

    def decrement_currency(currency_code, value)
      self.currency_map[currency_code] ||= 0
      self.currency_map[currency_code] -= value
    end
  end
end