require 'active_record'

class CreatePlayerWalletMigration < ActiveRecord::Migration[6.1]

  enable_extension 'pgcrypto' unless extension_enabled?('pgcrypto')

  def change
    create_table :player_wallets, id: :uuid do |t|
      t.belongs_to :user, type: :uuid, foreing_key: true

      t.jsonb :currency_map, default: {}

      t.timestamps null: false
    end
  end
end
