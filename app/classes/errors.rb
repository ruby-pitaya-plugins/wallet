module Wallet

  class InvalidCurrencyCodeError < RubyPitaya::RouteError
    def initialize(message)
      super(StatusCodes::CODE_ERROR_INVALID_CURRENCY_CODE, message)
    end
  end
end