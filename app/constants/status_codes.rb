module Wallet
  class StatusCodes

    CODE_OK    = RubyPitaya::StatusCodes::CODE_OK
    CODE_ERROR = RubyPitaya::StatusCodes::CODE_ERROR

    CODE_ERROR_INVALID_CURRENCY_CODE = 'RP-WALLET-001'

    def self.message_error_invalid_currency_code(currency_code)
      "Invalid currency code '#{currency_code}'"
    end

    ################
    ## Existent Codes
    ################
    ## Success codes
    #
    # RubyPitaya::StatusCodes::CODE_OK                   = 'RP-200'
    #
    #
    ## Error codes
    # RubyPitaya::StatusCodes::CODE_ERROR                = 'RP-201'
    # RubyPitaya::StatusCodes::CODE_UNKNOWN              = 'RP-000'
    # RubyPitaya::StatusCodes::CODE_HANDLER_NOT_FOUND    = 'RP-001'
    # RubyPitaya::StatusCodes::CODE_ACTION_NOT_FOUND     = 'RP-002'
    # RubyPitaya::StatusCodes::CODE_NOT_AUTHENTICATED    = 'RP-003'
    # RubyPitaya::StatusCodes::CODE_AUTHENTICATION_ERROR = 'RP-004'
    #
    # RubyPitaya::StatusCodes::Connector::CODE_UNKNOWN   = 'PIT-000'
    ################
  end
end
