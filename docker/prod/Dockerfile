FROM ruby:3.0.3-slim as builder

RUN apt update && \ 
    apt install -y --no-install-recommends \ 
    curl \
    libpq-dev \
    build-essential \
    && rm -rf /var/lib/apt/lists/*

COPY Gemfile Gemfile.lock ./

RUN bundle config --global jobs 4 && \
    bundle config --global set clean 'true' \
    bundle config --global git.allow_insecure true && \
    bundle config --global without "development test" && \
    bundle install

RUN rm -rf /usr/local/bundle/cache && \
    rm -rf /usr/local/bundle/build_info && \
    rm -rf /usr/local/bundle/doc && \
    rm -rf /usr/local/bundle/doc && \
    find /usr/local/bundle/gems/grpc-*/src/ruby/lib/grpc/2.5/ -name "*.so" -delete && \
    find /usr/local/bundle/gems/grpc-*/src/ruby/lib/grpc/2.6/ -name "*.so" -delete && \
    find /usr/local/bundle/gems/grpc-*/src/ruby/lib/grpc/2.7/ -name "*.so" -delete && \
    find /usr/local/bundle/gems/ -name "*.c" -delete && \
    find /usr/local/bundle/gems/ -name "*.o" -delete && \
    find /usr/local/bundle/gems/ -name "spec" -exec rm -rv {} +

# RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl

FROM ruby:3.0.3-slim

ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8

RUN apt update && \ 
    apt install -y --no-install-recommends \ 
    netcat \
    libpq5 \
    && rm -rf /var/lib/apt/lists/*

COPY --from=builder /usr/local/bundle /usr/local/bundle

# COPY --from=builder /kubectl /usr/local/bin/kubectl
# RUN chmod +x /usr/local/bin/kubectl

WORKDIR /app/rubypitaya/

COPY . .

ENTRYPOINT ["./docker/entrypoint.sh"]

CMD ["bundle", "exec", "rubypitaya", "run"]